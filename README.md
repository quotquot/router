# Quotquot's URL router

This is a simple URL router for the browser that creates web components on demand. To use, simply install the package with:

```shell
npm install @quotquot/router
```

and import the module in your code:

```javascript
import "@quotquot/router";
```

This will define four custom elements:

```
hash-router
default-route
pattern-route
static-route
```

which can be used to create web components on demand using rules such as:

```html
<hash-router>
  <static-route element="app-homepage" path="" keep></static-route>
  <pattern-route element="app-foo" regexp="^#/foo/(?<id>\d+)/?$"></pattern-route>
  <pattern-route element="app-bar" regexp="^#/bar/(?<id>\d+)/?$"></pattern-route>
  <default-route>
    <h1>unknown page</h1>
  </default-route>
</hash-router>
```
