require('./dist');

customElements.define('test-manager', class extends HTMLElement {
    constructor() {
        super();
        const p = document.createElement('p');
        p.textContent = 'It works!';
        this.appendChild(p);
    }
});

test('custom elements in JSDOM', () => {
    document.body.innerHTML = `<h1>Custom element test</h1> <test-manager></test-manager>`;
    expect(document.body.innerHTML).toContain('It works!');
});
