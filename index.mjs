/*
 * Copyright 2020-2021 Johnny Accot
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *      http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import logging from "logging";

const logger = logging.getLogger("@quotquot/router");
logger.setLevel("WARN");

export default class BaseRouteElement extends HTMLElement {

    constructor() {
        super();
        this.attachShadow({ mode: "open" });
        this.shadowRoot.appendChild(document.createElement("slot"));
    }

    show() {
        this.shadowRoot.firstChild.style.removeProperty("display");
    }

    hide() {
        this.shadowRoot.firstChild.style.setProperty("display", "none", "important");
    }
}

class DefaultRouteElement extends BaseRouteElement {
    accepts(hash) {
        this.show();
        return true;
    }
}

window.customElements.define("default-route", DefaultRouteElement);


class BaseFactoryRouteElement extends BaseRouteElement {
    hide() {
        super.hide();
        if (!this.hasAttribute("keep")) {
            const child = this.firstElementChild;
            if (child)
                this.removeChild(child);
        }
    }
}


class StaticRouteElement extends BaseFactoryRouteElement {
    accepts(hash) {
        const element = this.getAttribute("element");

        if (!element || hash !== this.getAttribute("path"))
            return false;

        if (!this.hasAttribute("keep") || !this.firstElementChild)
            this.appendChild(document.createElement(element));

        this.show();
        return true;
    }
}

window.customElements.define("static-route", StaticRouteElement);


function buildGroups(groups, matches) {
    const result = {};

    if (groups === null) {
        for (let i = 0; i < matches.length; ++i)
            result[`data-group-${i}`] = matches[i];
    }
    else {

        const groupNames = groups.split(",");

        if (groupNames.length !== matches.length)
            throw new Error("incorrect number of group names");

        for (let i = 0; i < matches.length; ++i)
            result[groupNames[i]] = matches[i];
    }

    return result;
}


class PatternRouteElement extends BaseFactoryRouteElement {
    accepts(hash) {
        const regexp = this.getAttribute("regexp");
        const element = this.getAttribute("element");

        /* if either of the two attributes is absent, the route is not active */
        if (!regexp || !element)
            return false;

        const re = new RegExp(regexp, "u");

        /* if the regexp does not match the hash, the route is not active */
        const match = re.exec(window.location.hash);

        if (!match)
            return false;

        /* if the browser supports named capture groups, use it, or create them */
        const groups = match.groups || buildGroups(
            this.getAttribute("groups"), match.splice(1)
        );
        const entries = Object.entries(groups);

        let child = this.firstElementChild;

        if (child) {
            for (const [k, v] of entries) {
                if (child.getAttribute(k) !== v)
                    child.setAttribute(k, v);
            }
        }
        else {
            child = document.createElement(element);
            entries.forEach(entry => child.setAttribute(...entry));
            this.appendChild(child);
            this.show();
        }

        return true;
    }
}

window.customElements.define("pattern-route", PatternRouteElement);


class HashRouterElement extends HTMLElement {

    constructor() {
        super();
        this.attachShadow({ mode: "open" });

        const slot = document.createElement("slot");

        function check(e) {
            const hash = window.location.hash;
            let none = true;
            for (const el of slot.assignedElements()) {
                if (el instanceof BaseRouteElement) {
                    if (none && el.accepts(hash))
                        none = false;
                    else
                        el.hide();
                }
                else
                    logger.warn("hash-router: slot is not a route");
            }
        }

        slot.addEventListener("slotchange", check);
        this.shadowRoot.appendChild(slot);

        window.addEventListener("hashchange", check, false);
    }
}

window.customElements.define("hash-router", HashRouterElement);
